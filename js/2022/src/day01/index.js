import run from "aocrunner"

const parseInput = (rawInput) => rawInput

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  const elves = input.split("\n\n").map((e) => e.split("\n"))
  const results = elves.reduce((mostCal, items) => {
    const total = items.reduce((total, item) => total + parseInt(item), 0)

    return mostCal < total ? total : mostCal
  }, -1)

  return results
}

const part2 = (rawInput) => {
  const input = parseInput(rawInput)
  const elves = input.split("\n\n").map((e) => e.split("\n"))
  const topThree = elves
    .map((items) => items.reduce((total, item) => total + parseInt(item), 0))
    .sort((a, b) => b - a)
    .slice(0, 3)

  return topThree.reduce((total, n) => total + n)
}

run({
  part1: {
    tests: [
      // {
      //   input: ``,
      //   expected: "",
      // },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      // {
      //   input: ``,
      //   expected: "",
      // },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
})
